<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* DocumentationDone/allDoc.html */
class __TwigTemplate_efe04133291b21f9c2481f2d7881b96254aad71d7b2311d7a1a6796d7ef6f918 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "__wraper/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("__wraper/index.html", "DocumentationDone/allDoc.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"container\">
        <div class=\"formcl\">
            ";
        // line 6
        echo twig_escape_filter($this->env, ($context["session"] ?? null), "html", null, true);
        echo "
            <form  method=\"POST\">
                <input type=\"text\" name=\"searchVal\" placeholder=\"pretraga...\" id=\"inputSearch\" autocomplete=\"off\" >
                <button type=\"submit\" name=\"userpanel\">Korisnicki Panel</button>
                <button type=\"submit\" name=\"logout\">Odjavi se</button>

            </form>
        </div>
        <div id=\"resltDoc\">
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["alldoc"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["alld"]) {
            // line 16
            echo "
            <div class=\"documentation\">

                <div><a href=\"doc/";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["alld"], "number_documentation", [], "any", false, false, false, 19), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["alld"], "number_documentation", [], "any", false, false, false, 19), "html", null, true);
            echo "</a> </div>
                <div>";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["alld"], "quantity", [], "any", false, false, false, 20), "html", null, true);
            echo "</div>
                <div>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["alld"], "created_at", [], "any", false, false, false, 21), "html", null, true);
            echo "</div>
                <div>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["alld"], "username", [], "any", false, false, false, 22), "html", null, true);
            echo "</div>

            </div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['alld'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        </div>
    </div>
";
    }

    // line 31
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "    <script src=\"/stamparija/assets/javascript/serarchDoc.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "DocumentationDone/allDoc.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 32,  107 => 31,  101 => 27,  90 => 22,  86 => 21,  82 => 20,  76 => 19,  71 => 16,  67 => 15,  55 => 6,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "DocumentationDone/allDoc.html", "C:\\xampp\\htdocs\\stamparija\\views\\DocumentationDone\\allDoc.html");
    }
}
