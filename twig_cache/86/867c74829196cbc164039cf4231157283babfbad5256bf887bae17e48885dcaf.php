<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* User/getUser.html */
class __TwigTemplate_8a54f1b8b739daae1c17cf9075ab7224c37003d77f3dae38aa65b217a3419193 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "__wraper/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("__wraper/index.html", "User/getUser.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "
   <div class=\"userprepare\">
      <div>";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userprepare"] ?? null), "username", [], "any", false, false, false, 5), "html", null, true);
        echo "</div>
      <div>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userprepare"] ?? null), "forname", [], "any", false, false, false, 6), "html", null, true);
        echo "</div>
      <div>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userprepare"] ?? null), "surname", [], "any", false, false, false, 7), "html", null, true);
        echo "</div>
   </div>

      <form method=\"POST\">
         <div class=\"pssword\">
            <input type=\"text\"  class=\"inputpass\" name=\"username\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userprepare"] ?? null), "username", [], "any", false, false, false, 12), "html", null, true);
        echo "\">
            <input type=\"password\" class=\"inputpass\" name=\"password\" placeholder=\"enter your password\">
            <button type=\"submit\">Prijavi se</button>
         </div>
      </form>



";
    }

    public function getTemplateName()
    {
        return "User/getUser.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 12,  62 => 7,  58 => 6,  54 => 5,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "User/getUser.html", "C:\\xampp\\htdocs\\stamparija\\views\\User\\getUser.html");
    }
}
