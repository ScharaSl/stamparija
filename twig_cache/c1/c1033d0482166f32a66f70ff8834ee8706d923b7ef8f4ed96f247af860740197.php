<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* UserPanel/getUserPanel.html */
class __TwigTemplate_e9a85eaf1f3184356f649d882807826465a21e733be3c856a621b624d434450b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "__wraper/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("__wraper/index.html", "UserPanel/getUserPanel.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"container\">
       <div class=\"content\">
           <div class=\"user\"> ";
        // line 6
        echo twig_escape_filter($this->env, ($context["session"] ?? null), "html", null, true);
        echo "</div>
          <form method=\"POST\">
              <input type=\"text\" placeholder=\"unesite broj naloga...\" autocomplete=\"off\" name=\"numberdoc\" pattern=\"[0-9]{12}\" id=\"numberdocumnetation\" title=\"Broj naloga mora sadrzaati tacno 12 brojeva\">
              <input type=\"number\" placeholder=\"unesite kolicinu naloga...\" name=\"sumdoc\" min=\"1\" step=\"1\" value=\"1\">
              <select name=\"define\">
                  <option value=\"1\">Gorenje</option>
                  <option value=\"2\">Smeg</option>
                  <option value=\"3\">Hisense</option>
                  <option value=\"4\">Franke</option>
                  <option value=\"5\">Sumit</option>
              </select>
              <input type=\"text\" placeholder=\"upsi serijski broj specijalnog kupca...\" autocomplete=\"off\" name=\"specnumberdoc\">
              <div id=\"allcheck\">
              <div class=\"checkboxdoc\">
                  <div class=\"checknumber\">
                      <input type=\"checkbox\" name=\"number348805\" id=\"num348805\" value=\"1\">348805
                      <input type=\"number\" name=\"number348805s\" class=\"number\" min=\"1\" step=\"1\" value=1>
                  </div>
                  <div class=\"checknumber\">
                      <input type=\"checkbox\" name=\"number348804\">348804
                      <input type=\"number\" name=\"number348804s\" class=\"number\" min=\"1\" step=\"1\" value=\"1\">
                  </div>
                  <div class=\"checknumber\">
                      <input type=\"checkbox\" name=\"number656361\">656361
                      <input type=\"number\" name=\"number656361s\" class=\"number\" min=\"1\" step=\"1\" value=\"1\">
                  </div>
                  <div class=\"checknumber\">
                      <input type=\"checkbox\" name=\"number327082\">327082
                      <input type=\"number\" name=\"number327082s\" class=\"number\" min=\"1\" step=\"1\" value=\"1\">
                  </div>
                  <div class=\"checknumber\">
                      <input type=\"checkbox\" name=\"number523341\">523341
                      <input type=\"number\" name=\"number523341s\" class=\"number\" min=\"1\" step=\"1\" value=\"2\">
                  </div>
              </div>
              <div class=\"checkboxdoc\">

              </div>
                  <div class=\"checkboxdoc\">

                  </div>
              </div>
              <button type=\"submit\">Upisi nalog</button>
          </form>
           <input type=\"button\" value=\"Proveri kolicine\" id=\"checkSum\">
       </div>
        <div id=\"prewDocSum\">
           <div id=\"prewDocMain\">
            <div class=\"doc\">

            </div>
           </div>
            <div class=\"btn\">
                <input type=\"button\" value=\"Zatvori\" id=\"closeInfo\">
            </div>
        </div>
    </div>
";
    }

    // line 65
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "        <script src=\"/stamparija/assets/javascript/regularexpre.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "UserPanel/getUserPanel.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 66,  117 => 65,  55 => 6,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "UserPanel/getUserPanel.html", "C:\\xampp\\htdocs\\stamparija\\views\\UserPanel\\getUserPanel.html");
    }
}
