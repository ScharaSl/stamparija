<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Admin/getAdmin.html */
class __TwigTemplate_47cb3765c8457b67e78b9821935486d84a60e2f651aa44aed03d1b70008a687d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "__wraper/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("__wraper/index.html", "Admin/getAdmin.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "   <div class=\"admin\">
      <form class=\"adminform\" method=\"POST\">
         <div class=\"formdata\">
            <label for=\"forname\">Ime</label>
            <input type=\"text\" id=\"forname\" placeholder=\"Ime\" name=\"forname\" required>
         </div>
         <div class=\"formdata\">
            <label for=\"surname\">Prezime</label>
            <input type=\"text\" id=\"surname\" placeholder=\"Prezime\" name=\"surname\" required>
         </div>
         <div class=\"formdata\">
            <label for=\"username\">Korisincko Ime</label>
            <input type=\"text\" id=\"username\" placeholder=\"Korisnicko Ime\" name=\"username\" required>
         </div>
         <div class=\"formdata\">
            <label for=\"email\">Email</label>
            <input type=\"email\" id=\"email\" placeholder=\"Email\" name=\"email\">
         </div>
         <div class=\"formdata\">
            <label for=\"phone\">Telefon</label>
            <input type=\"text\" id=\"phone\" placeholder=\"Telefon\" name=\"phone\">
         </div>
         <div class=\"formdata\">
            <label for=\"password\">Lozinka</label>
            <input type=\"password\" id=\"password\" placeholder=\"Lozinka\" name=\"password\" required>
         </div>
         <div class=\"formdata\">
            <label for=\"password_again\">Lozinka</label>
            <input type=\"password\" id=\"password_again\" placeholder=\"Ponovi Lozinku\" name=\"password_again\" required>
         </div>
         <div class=\"formdata\">
            <button type=\"submit\" class=\"buttonreg\">Registruj Korisnika</button>
         </div>
      </form>
   </div>
";
    }

    public function getTemplateName()
    {
        return "Admin/getAdmin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Admin/getAdmin.html", "C:\\xampp\\htdocs\\stamparija\\views\\Admin\\getAdmin.html");
    }
}
