<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __wraper/index.html */
class __TwigTemplate_40bf8b9704f6ab6cd01d9c9baf1f0a597bf16b832c23eb3acabf5ad8286a3e00 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Deployment...</title>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"/stamparija/assets/css/main.css\">
    <script src=\"https://code.jquery.com/jquery-3.6.0.min.js\" integrity=\"sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=\" crossorigin=\"anonymous\"></script>

</head>
<body>
    <div class = \"container\">
        ";
        // line 12
        $this->displayBlock('main', $context, $blocks);
        // line 14
        echo "    </div>

    ";
        // line 16
        $this->displayBlock('javascript', $context, $blocks);
        // line 18
        echo "
</body>
</html>";
    }

    // line 12
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "        ";
    }

    // line 16
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
    }

    public function getTemplateName()
    {
        return "__wraper/index.html";
    }

    public function getDebugInfo()
    {
        return array (  78 => 17,  74 => 16,  70 => 13,  66 => 12,  60 => 18,  58 => 16,  54 => 14,  52 => 12,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__wraper/index.html", "C:\\xampp\\htdocs\\stamparija\\views\\__wraper\\index.html");
    }
}
