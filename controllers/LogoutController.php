<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 16/07/2021
 * Time: 00:50
 */

namespace App\Controllers;


use App\Core\Controller;

class LogoutController extends  Controller {
    public function logout(){
        session_unset();
        session_destroy();
        //header("Location:/stamparija/", true);
    }
}