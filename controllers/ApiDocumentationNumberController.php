<?php


namespace App\controllers;
use App\Core\ApiController;

class ApiDocumentationNumberController extends ApiController {

    public function postNumberDocApi(){
        $dateCurent = date("Y/m/d");

        $documentationNumberDone = new \App\Models\DocumentationNumberModels($this->getDbc());
        //$documentation = $documentationNumberDone->getLikeFileName("date", $dateCurent);
        $documentation = $documentationNumberDone->getSumQuantityDocumentation($dateCurent);
        $this->set("documentationNumberDone", $documentation);


    }
}