<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 20/07/2021
 * Time: 20:33
 */

namespace App\Controllers;
use App\Core\ApiController;
use App\Core\Controller;

class ApiSearchController extends ApiController {

    public function getSearch(){

    }
    
    public function postSerach($value){
        $searchIns = new \App\Models\DocumentationDoneModels($this->getDbc());
        $search = $searchIns->postSearchDocumentation($value);
        $this->set("search", $search);
    }
}