<?php


namespace App\Controllers;
use App\Core\Controller;

class AdminController extends Controller {
    public function getAdmin(){

    }

    public function postAdmin(){
        $forname = filter_input(INPUT_POST, "forname", FILTER_SANITIZE_STRING);
        $surname = filter_input(INPUT_POST, "surname", FILTER_SANITIZE_STRING);
        $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
        $phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
        $passwordAgain = filter_input(INPUT_POST, "password_again", FILTER_SANITIZE_STRING);

        if($password != $passwordAgain){
            $this->set("message", "Paswword mora u oba polja biti identican");
            return;
        }

        $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        $adminaAdduser = new \App\Models\AdminModels($this->getDbc());
        $data = $adminaAdduser->addUser(
            $forname,
            $surname,
            $username,
            $email,
            $phone,
            $passwordHash
        );
       $this->set("message", $data);
    }
}