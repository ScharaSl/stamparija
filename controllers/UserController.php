<?php



namespace App\Controllers;
use App\Core\Controller;

//session_start();

class UserController extends Controller{

    public function getUser($id){
       $user = new \App\Models\UserModels($this->getDbc());
       $data = $user->getById($id);
       $this->set("userprepare", $data);
    }

    public function postUser(){
        $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

        $checkPasswordLength = (new \App\Validators\StringValidators())->minLength(7);

        if(!$checkPasswordLength->isValid($password)){
            $this->set("message", "Lozinka ima neispravan broj karaktera...");
            return;
        }

        $userModel = new \App\Models\UserModels($this->getDbc());
        $userNameCheck = $userModel->getFillname("username", $username);

        if(!$userNameCheck){
            $this->set("message", "Korisnik sa ovim korisnickim imenom ne postoji");
            return;
        }

        $passworHash = $userNameCheck->password_hesh;

        if(!password_verify($password, $passworHash)){
            $this->set("message", "Password nije isparavan...");
            return;
        }
        if(!isset($_SESSION["usernamelog"] )){
            $_SESSION["usernamelog"] = filter_var($username, FILTER_SANITIZE_STRING);
        }

        $this->redirection("docdon");
    }
}