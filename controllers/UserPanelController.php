<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 28/07/2021
 * Time: 17:25
 */

namespace App\Controllers;


use App\Core\Controller;


class UserPanelController extends Controller{

    public function getUserPanel(){

    }

    public function  postUserPanel(){
         $numberdoc = filter_input(INPUT_POST, "numberdoc", FILTER_SANITIZE_STRING);
         $sumdoc = filter_input(INPUT_POST, "sumdoc", FILTER_SANITIZE_NUMBER_INT);
         $define = filter_input(INPUT_POST, "define", FILTER_SANITIZE_NUMBER_INT);
         $specnumberdoc = filter_input(INPUT_POST, "specnumberdoc", FILTER_SANITIZE_STRING);
         if($specnumberdoc == "") $specnumberdoc = "Ne postoji podatak za ovaj nalog...";

         $user = $_SESSION["usernamelog"];

         $userModel = new \App\Models\UserModels($this->getDbc());
         $checkeUsername = $userModel->getFillname("username", $user);
         $userId = (int) $checkeUsername->user_id;


         if(!$checkeUsername){
             $this->set("message", "Ovaj korisniki ne moze da pristupi bazi podataka...");
             return;
         }

        #add documentaion done
        $documentationDone = new \App\Models\DocumentationDoneModels($this->getDbc());

         $addDoc = $documentationDone->add([
             "user_id" => $userId,
             "quantity" => $sumdoc,
             "number_documentation" => $numberdoc,
             "brand_id" => $define,
             "spec_number_doc" => $specnumberdoc
         ]);


         #add number documentation

        $documentNumberModel = new \App\Models\DocumentationNumberModels($this->getDbc());
        $date = date("Y/m/d");

        if(isset($_POST["number348805"])){
            $number = "348805";
            $quantity = filter_input(INPUT_POST, "number348805s", FILTER_SANITIZE_NUMBER_INT) * $sumdoc;

            $documentNumberModel->add([
                "doc_number" => $number,
                "date" => $date,
                "quantity" => $quantity
            ]);
        }

        if(isset($_POST["number348804"])){
            $number = "348804";
            $quantity = filter_input(INPUT_POST, "number348804s", FILTER_SANITIZE_NUMBER_INT) * $sumdoc;

            $documentNumberModel->add([
                "doc_number" => $number,
                "date" => $date,
                "quantity" => $quantity
            ]);
        }

        if(isset($_POST["number656361"])){
            $number = "656361";
            $quantity = filter_input(INPUT_POST, "number656361s", FILTER_SANITIZE_NUMBER_INT) * $sumdoc;

            $documentNumberModel->add([
                "doc_number" => $number,
                "date" => $date,
                "quantity" => $quantity
            ]);
        }

        if(isset($_POST["number327082"])){
            $number = "327082";
            $quantity = filter_input(INPUT_POST, "number327082s", FILTER_SANITIZE_NUMBER_INT) * $sumdoc;

            $documentNumberModel->add([
                "doc_number" => $number,
                "date" => $date,
                "quantity" => $quantity
            ]);
        }

        if(isset($_POST["number523341"])){
            $number = "523341";
            $quantity = filter_input(INPUT_POST, "number523341s", FILTER_SANITIZE_NUMBER_INT) * $sumdoc;

            $documentNumberModel->add([
                "doc_number" => $number,
                "date" => $date,
                "quantity" => $quantity
            ]);
        }




        if($addDoc){
             $this->redirection("userpanel");
         }

         //sum documentation




    }
}