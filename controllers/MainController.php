<?php
namespace App\Controllers;
use App\Core\Controller;

class MainController extends Controller {

    public function home(){
        $allUsers = new \App\Models\UserModels($this->getDbc());
        $getUser = $allUsers->getAll();
        $this->set("allusers", $getUser);
    }

}