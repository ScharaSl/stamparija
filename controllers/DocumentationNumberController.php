<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 16/08/2021
 * Time: 13:50
 */

namespace App\controllers;


use App\Core\ApiController;
use App\Core\Controller;

class DocumentationNumberController extends ApiController {
    public function getNumberDoc(){

    }

    public function postNumberDoc(){
        $docNumber = new \App\Models\DocumentationNumberModels($this->getDbc());
        $documentation = $docNumber->getAll();
        $this->set("documentationNumberDone", $documentation);
    }
}