<?php


namespace App\Controllers;
use \App\Core\Controller;

class DocumentationDoneController extends Controller{

    public function allDoc(){
       $allDoc = new \App\Models\DocumentationDoneModels($this->getDbc());
       $data = $allDoc->getUser();

       $this->set("alldoc", $data);
    }

    public function manipulatedDoc(){
        if(isset($_POST["logout"])){
            $logour = new \App\Controllers\LogoutController($this->getDbc());
            $logour->logout();
            $this->redirection("");
        }

        if(isset($_POST["userpanel"])){
            $this->redirection("userpanel");
        }
    }


    public function addDocumentation(){

    }

}