<?php


namespace App\Controllers;


use App\Core\Controller;

class DocumentationManController extends Controller {

    public function show($numberDoc){
        $documetation = new \App\Models\DocumentationDoneModels($this->getDbc());
        $getDoc = $documetation->getFillname("number_documentation", $numberDoc);
        $this->set("docdata", $getDoc);

        $brand = $documetation->getDocumentationman($numberDoc);
        $this->set("brand", $brand);

        $user = $documetation->getUser($numberDoc);
        $this->set("user", $user);


    }
}