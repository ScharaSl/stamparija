<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 28/07/2021
 * Time: 16:49
 */

namespace App\Validators;


use App\Core\Validator;

class IntegerVlidator implements  Validator {
    private $integerLength;
    private $integerDecimal;

    public function __construct(){
        $this->integerLength=1;
        $this->integerDecimal = 0;
    }

    public function setIntegerLength($length):IntegerVlidator{
        $this->integerLength = max(1, $length);
        return $this;
    }

    public function setDecimal($decimal):IntegerVlidator{
        $this->integerDecimal = max(0, $decimal);
        return $this;
    }
    
    public function isValid($value):bool {
        $patern = "|^";

        $patern .= "[1-9][0-9]{0,".$this->integerLength."}";

        if($this->integerDecimal !=0){
            $patern .= "\.[1-9]{1,".$this->integerDecimal."}";
        }

        $patern .= "$|";

        return boolval(preg_match($patern, $value));
    }
}