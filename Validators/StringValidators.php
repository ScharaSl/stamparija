<?php

namespace App\Validators;

use App\Core\Validator;

class StringValidators implements Validator {
    private $value;
    private $minlength;
    private $maxlength;

    public function __construct(){
        $this->minlength = 2;
        $this->maxlength = 255;
    }

    public function minLength($length):StringValidators{
        $this->minlength = $length;
        return $this;
    }

    public function maxlength($length):StringValidators{
        $this->maxlength = $length;
        return $this;
    }

    public function isValid($value):bool{
        $len = strlen($value);

        return boolval($this->minlength <= $len && $this->maxlength >= $len);
    }
}