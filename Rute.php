<?php


return [
    App\Core\Routes::get( "|^administrator/schara$|",  "Admin"                  , "getAdmin"),
    App\Core\Routes::post("|^administrator/schara$|",  "Admin"                  , "postAdmin"),

    App\Core\Routes::get( "|^docdon$|"              , "DocumentationDone"       , "allDoc"),
    App\Core\Routes::post("|^docdon$|"              , "DocumentationDone"       , "manipulatedDoc"),
    App\Core\Routes::get("|^doc/([0-9]+)+/?$|"      , "DocumentationMan"        , "show"),

    App\Core\Routes::get("|^userpanel|"             , "UserPanel"               , "getUserPanel"),
    App\Core\Routes::post("|^userpanel|"            , "UserPanel"               , "postUserPanel"),



    App\Core\Routes::get( "|^user/([0-9]+)+/?$|"    , "User"                    , "getUser"),
    App\Core\Routes::post("|^user/([0-9]+)+/?$|"    , "User"                    , "postUser"),
    #Api
    App\Core\Routes::get( "|^search/?$|"            , "ApiSearch"               , "getSearch"),
    App\Core\Routes::post("|^search/([0-9]+)/?$|"   , "ApiSearch"               , "postSerach"),
    App\Core\Routes::post("|^docnumdon/?$|"         , "ApiDocumentationNumber"  , "postNumberDocApi"),

    App\Core\Routes::get( "|^logout/?$|"            , "Logout"                  , "logout"),

    App\Core\Routes::any( "|^.*$|"                  , "Main"                    , "home"),
];