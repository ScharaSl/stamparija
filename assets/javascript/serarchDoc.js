
function search(){
    var input = document.getElementById("inputSearch").value;
    document.getElementById("inputSearch").setAttribute("autocomplete", "off");
    fetch("http://localhost/stamparija/search/" + input, {method:"POST"},{credentials: "include"})
        .then(result => result.json())
        .then(data =>{
            setResult(data);
            console.log(data);
        })

}

function setResult(data){
    let divelement = document.querySelector("#resltDoc");
    divelement.innerHTML = "";
        if(data.search.length != 0){
            console.log("Ima podataka...");
            let divResult = document.createElement("div");
            for(let dat of data.search){
                if(typeof dat === "object"){
                    divResult.innerHTML += "<a href='#'> " + dat.number_documentation + "</a>" ;
                }
            }
            divResult.classList.add("resultSearch");
            divelement.appendChild(divResult);
        }
        if(data.search.length == 0){
            divelement.innerHTML = "";
        }

}

addEventListener("keyup", search);