addEventListener("load", main);

function main(){
    document.getElementById("checkSum").addEventListener("click", getInfoDoc);
    document.getElementById("closeInfo").addEventListener("click", closeInfo);
}

function getInfoDoc(){
    let prewDoc = document.getElementById("prewDocSum");
    prewDoc.style.display = "block";
    fetch("http://localhost/stamparija/docnumdon", {method:"POST"}, {credential: "include"})
        .then(result => result.json())
        .then(data => {
            setPopUpData(data);
        })
}

function closeInfo(){
    let prewDoc = document.getElementById("prewDocSum");
    prewDoc.style.display = "none";
}

function setPopUpData(data){
    let docmain = document.getElementById("prewDocMain");
    docmain.innerText = "";
    for(var i = 0; i < data.documentationNumberDone.length; i++){
        let doc = document.createElement("div");
        let number = document.createElement("div");
        let sum = document.createElement("div");



        doc.classList.add("doc");
        number.classList.add("number");
        sum.classList.add("sum");

        number.innerText= data.documentationNumberDone[i].doc_number;
        sum.innerText = data.documentationNumberDone[i].quntity;

        doc.appendChild(number);
        doc.appendChild(sum);

        docmain.appendChild(doc);
    }


}