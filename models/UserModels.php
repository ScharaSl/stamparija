<?php
namespace  App\Models;
use App\Core\Field;
use App\Core\Fileds;
use App\Core\Model;
use App\Validators\StringValidators;

class UserModels extends Model{
   
    protected function getFilds(){
        return[
            "forname"       => new Fileds((new StringValidators())->maxlength(30),true),
            "surname"       => new Fileds((new StringValidators())->maxlength(30),true),
            "username"      => new Fileds((new StringValidators())->maxlength(128),true),
            "email"         => new Fileds((new StringValidators())->maxlength(128),true),
            "phone_number"  => new Fileds((new StringValidators())->maxlength(128),true),
            "password_hesh" => new Fileds((new StringValidators())->maxlength(256),true),
        ];
    }


}