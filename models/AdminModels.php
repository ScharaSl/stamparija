<?php


namespace App\Models;
use App\Core\Fileds;
use App\Core\Model;
use App\Validators\StringValidators;

class AdminModels extends Model {

    public function getFilds(){
       return [
           "forname"      => new Fileds((new StringValidators())->minLength(30)->maxlength(30), true),
           "surname"      => new Fileds((new StringValidators())->minLength(30)->maxlength(30), true),
           "username"     => new Fileds((new StringValidators())->minLength(30)->maxlength(30), true),
           "phone_number" => new Fileds((new StringValidators())->minLength(30)->maxlength(30), true),
           "email"        => new Fileds((new StringValidators())->minLength(30)->maxlength(30), true),
       ];
    }

    public function addUser($forname, $surname, $username, $emailm, $phone, $passwrd){
        $sql = "INSERT INTO user (forname,surname,username,email,phone_number,password_hesh)
                VALUES (?,?,?,?,?,?)";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute([$forname, $surname, $username, $emailm, $phone, $passwrd]);
        $result = "Doslo je do grski pri registraciji";
        if($execute){
            $result = "Novi korisnik je uspesno registrovan";
        }

        return $result;
    }
}