<?php

namespace App\Models;
use App\Core\Fileds;
use App\Core\Model;
use App\Validators\IntegerVlidator;
use App\Validators\StringValidators;

class DocumentationDoneModels extends Model{

    public function getFilds(){
        return [
            "user_id"              => new Fileds((new IntegerVlidator())->setIntegerLength(10), true),
            "quantity"             => new Fileds((new IntegerVlidator())->setIntegerLength(20), true),
            "number_documentation" => new Fileds((new StringValidators())->maxlength(50), true),
            "brand_id"             => new Fileds((new IntegerVlidator())->setIntegerLength(10), true),
            "spec_number_doc"      => new Fileds((new StringValidators())->maxlength(50), true)

        ];
    }


    public function getUser($value = NULL){
        if($value == NULL){
            $sql = "
        SELECT documentation_done.quantity,documentation_done.number_documentation,documentation_done.created_at,user.username
        FROM documentation_done
        JOIN user ON documentation_done.user_id = user.user_id
        ORDER BY documentation_done.created_at DESC
        ";

            $prepere = $this->getConnection()->prepare($sql);
            $execute = $prepere->execute();
            $result = [];

            if($execute){
                $result =  $prepere->fetchAll(\PDO::FETCH_OBJ);
            }

            return $result;
        }

        $sql = "
        SELECT username
        FROM user
        JOIN documentation_done ON documentation_done.user_id = user.user_id
        WHERE documentation_done.number_documentation = ?
        ";

        $prepere = $this->getConnection()->prepare($sql);
        $execute = $prepere->execute([$value]);
        $result = NULL;

        if($execute){
            $result =  $prepere->fetch(\PDO::FETCH_OBJ);
        }

        return $result;

    }


    public function postSearchDocumentation(int $value){

        $sql = "SELECT * FROM documentation_done WHERE number_documentation LIKE ?";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute(["%{$value}%"]);
        $result = [];

        if($execute){
            $result = $prepare->fetchAll(\PDO::FETCH_OBJ);
        }

        return $result;
    }

    public function getDocumentation(){

    }

    public function postDocumentation(){

    }

    public function getDocumentationman($numberDoc){
        $sql = "SELECT brand_name FROM brand 
                INNER JOIN documentation_done 
                ON brand.brand_id=documentation_done.brand_id
                WHERE documentation_done.number_documentation = ?";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute([$numberDoc]);
        $resul = null;

        if($execute){
            $resul = $prepare->fetch(\PDO::FETCH_OBJ);
        }

        return $resul;
    }


}