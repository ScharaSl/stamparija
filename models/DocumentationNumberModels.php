<?php


namespace App\Models;


use App\Core\Fileds;
use App\Core\Model;
use App\Validators\IntegerVlidator;
use App\Validators\StringValidators;

class DocumentationNumberModels extends Model{

    public function getFilds() {
        return [
            "doc_number" => new Fileds((new IntegerVlidator())->setIntegerLength(11), true),
            "date" => new Fileds((new StringValidators())->maxlength(50), true),
            "quantity" => new Fileds((new IntegerVlidator())->setIntegerLength(11), true)
        ];
    }

    public function getSumQuantityDocumentation($value){
        $sql = "SELECT doc_number,SUM(quantity) AS quntity FROM documentation_number  WHERE date = ? GROUP BY doc_number";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute([$value]);
        $result = [];

        if($execute){
            $result = $prepare->fetchAll(\PDO::FETCH_OBJ);
        }

        return $result;
    }

}