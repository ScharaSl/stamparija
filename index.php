<?php

require_once "vendor/autoload.php";
require_once "Configuration.php";
session_start();

$databaseConfiguration = new \App\Core\DatabaseConfiguration(
                                                    Configuration::HOST_NAME,
                                                    Configuration::USERNAME,
                                                    Configuration::PASSWORD,
                                                    Configuration::DBNAME);
$dbConnection = new \App\Core\DatabaseConnection($databaseConfiguration);

$userlogin = $_SESSION["usernamelog"] ?? "Nema prijavljenog korisnika...";

$url = filter_input(INPUT_GET, "URL");
$httpMethods = filter_input(INPUT_SERVER, "REQUEST_METHOD");

$routes = require_once "Rute.php";

$router = new \App\Core\Router();

foreach ($routes as $rout){
    $router->addRoute($rout);
}

$findRoute = $router->finde($httpMethods,$url);

$arguments = $findRoute->extratsArguments($url);

$controllerName= "\\App\\Controllers\\" . $findRoute->getController() ."Controller";
$controllerInstance = new $controllerName($dbConnection);

$methoName = $findRoute->getMethod();

call_user_func_array([$controllerInstance, $methoName], $arguments);

$data = $controllerInstance->getData();

$data["session"] = $userlogin;

if($controllerInstance instanceof \App\Core\ApiController){
    ob_clean();
    header("Content-type: application/json; charset=utf-8");
    header("Access-Control-Allow-Origin: *");

    print_r( json_encode($data));
    exit;
}

$loader = new \Twig\Loader\FilesystemLoader("./views");
$twig = new  \Twig\Environment($loader, [
    "cache" => "./twig_cache",
    "auto_reload" => true
]);


echo $twig->render( $findRoute->getController() . "/" . $findRoute->getMethod() . ".html", $data);

