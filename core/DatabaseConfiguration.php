<?php

namespace App\Core;

final class DatabaseConfiguration{

    private $hostname;
    private $username;
    private $password;
    private $dbname;

    public function __construct($hostname, $username, $password, $dbnaem){
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbnaem;
    }

    public function getSource(){
        return "mysql:host={$this->hostname};dbname={$this->dbname};charset=utf8";
    }

    public function getUsername(){
        return $this->username;
    }

    public function getPassword(){
        return $this->password;
    }

}