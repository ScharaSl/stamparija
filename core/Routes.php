<?php


namespace App\Core;


class Routes{
    private $pattern;
    private $requestMethod;
    private $method;
    private $controler;

    public function __construct($requestMethod,$pattern, $controler,$method){
        $this->pattern = $pattern;
        $this->requestMethod = $requestMethod;
        $this->method = $method;
        $this->controler = $controler;
    }

    public static function get($pattern, $controler, $methos){
        return new Routes("GET", $pattern,$controler,$methos);
    }

    public static function post($pattern, $controler, $methos){
        return new Routes("POST", $pattern,$controler,$methos);
    }

    public static function any($pattern, $controler, $methos){
        return new Routes("GET|POST", $pattern,$controler,$methos);
    }

    public function getController(){
        return $this->controler;
    }

    public function getMethod(){
        return $this->method;
    }

    public function metch($method, ?string $url) {
        if(!preg_match("/^".$this->requestMethod."$/", $method)){
            return false;
        }
        //return boolval(preg_match($this->pattern, $url));

        if(!preg_match($this->pattern , $url)){
            return false;
        }

        return true;
    }

    public function extratsArguments( ?string $url){
        $arguments = [];
        $metch = [];
        preg_match_all($this->pattern, $url, $metch);

        if(isset($metch[1])){
            $arguments = $metch[1];
        }

        return $arguments;

    }
}