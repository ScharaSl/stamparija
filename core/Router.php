<?php

namespace App\Core;


class Router{
    private $routs = [];

    public function addRoute(Routes $routes){
        $this->routs[]  = $routes;
    }
    public function finde($method, ?string $url){
        foreach ($this->routs as $route){
            if($route->metch($method,$url)){
              return $route;
            }
        }

        return  null;
    }

    public function getRote(){
        return $this->routs;
    }

}