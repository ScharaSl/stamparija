<?php


namespace App\Core;


class Model{

    private $dbc;

    public function __construct(DatabaseConnection $dbc){
        $this->dbc = $dbc;
    }

    public function getConnection(){
        return $this->dbc->getConnection();
    }

    protected function getFilds(){
        return [];
    }

    public function getTableName(){
        $className = static::class;
        $result = [];

        preg_match("|^App\\\Models\\\(([A-Z][a-z]+)+)Models$|", $className, $result);
        $classNameTable = $result[1] ?? "";
        $classNameTableUnderScore = preg_replace("|[A-Z]|", "_$0", $classNameTable);
        $classnameTableToLowerCase = strtolower($classNameTableUnderScore);
        $tableName = substr($classnameTableToLowerCase,1);

        return $tableName;
    }

    public function getAll(){
        $tableName = $this->getTableName();

        $sql = "SELECT * FROM {$tableName}";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute();
        $result = [];

        if($execute){
            $result = $prepare->fetchAll(\PDO::FETCH_OBJ);
        }

        return $result;
    }

    public function getById($id){
        $tableName = $this->getTableName();

        $sql = "SELECT * FROM {$tableName} WHERE user_id=?";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute([$id]);
        $result = null;

        if($execute){
            $result = $prepare->fetch(\PDO::FETCH_OBJ);
        }

        return $result;
    }

    public function isFilldValidValue($fildName, $value){
        $fild = $this->getFilds();
        $fildNameSuported = array_keys($fild);

        if(!in_array($fildName, $fildNameSuported)){
            return false;
        }

        return boolval($fild[$fildName]->isValid($value));
    }

    private function checkedInFileList($value){
        $data = $this->getFilds();

        $dataKey = array_keys($data);
        $valueKey = array_keys($value);

        foreach ($valueKey as $valK){
            if(!in_array($valK, $dataKey)){
                throw new \Exception("Not suported fild value: " . $valK);
            }

            if(!$data[$valK]->isEditable()){
                throw new \Exception("Field {$valK} is not editable");
            }

            if(!$data[$valK]->isValid($value[$valK])){
                throw new \Exception("Fild value {$valK} is not valid");
            }
        }


    }

    public function getFillname($filName, $value){
        $tableName = $this->getTableName();
        if(!$this->isFilldValidValue($filName, $value)){
            new \Exception("Error: " . $filName);
        }

        $sql = "SELECT * FROM " . $tableName . " WHERE " . $filName . "=?";
        $preape = $this->getConnection()->prepare($sql);
        $execute = $preape->execute([$value]);
        $result = null;

        if($execute){
            $result = $preape->fetch(\PDO::FETCH_OBJ);
        }

        return $result;
    }



    public function add($value){
       $this->checkedInFileList($value);

       $tablename = $this->getTableName();

       $impplodeFilds = implode(",", array_keys($value));
       $numberQustionmark = str_repeat("?,", count($value));
       $questionMark = substr($numberQustionmark, 0, -1);
       $sql = "INSERT INTO {$tablename} ({$impplodeFilds}) VALUES ({$questionMark})";
       $prepare = $this->getConnection()->prepare($sql);

       $execute = $prepare->execute(array_values($value));
       $result = null;

       if(!$execute){
           return false;
       }
       return true;
    }

    public function getLikeFileName($fileName,  $likeDate){
        if(!$this->isFilldValidValue($fileName,$likeDate)){
            new \Exception("Error: " . $fileName);
        }

        $tableName = $this->getTableName();

        $sql = "SELECT * FROM {$tableName} WHERE {$fileName} LIKE ?";
        $prepare = $this->getConnection()->prepare($sql);
        $execute = $prepare->execute(["%{$likeDate}%"]);
        $result = [];

        if($execute){
            $result = $prepare->fetchAll(\PDO::FETCH_OBJ);
        }

        return $result;
    }
}