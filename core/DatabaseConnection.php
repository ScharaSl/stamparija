<?php


namespace App\Core;


class DatabaseConnection{
    private $configuration;
    private $connection;

    public function __construct(DatabaseConfiguration $dbc){
        $this->configuration = $dbc;
    }

    public function getConnection(){
        if($this->connection == null){
            $this->connection = new \PDO($this->configuration->getSource(), $this->configuration->getUsername(), $this->configuration->getPassword());
        }

        return $this->connection;
    }

}