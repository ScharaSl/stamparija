<?php

namespace App\Core;


class Controller{
    private $dbc;
    private $data = [];

    public function __construct(DatabaseConnection $dbc){
        $this->dbc = $dbc;
    }

    public function getDbc(){
        return $this->dbc;
    }

    public function set($name, $value)   {
        $result = false;
        if(preg_match("/^[a-z][a-z0-9]+([A-Z][a-z0-9]+)*$/", $name)){
            $this->data[$name] = $value;
            $result = true;
        }

        return $result;
    }

    public function getData(){
        return $this->data;
    }

    public function dataPush($name, $dataNums){
        foreach ($dataNums as $datanum){
            $this->data[$name] = $datanum;
        }
    }

    public function redirection($value){
        ob_clean();
        header("Location:/stamparija/{$value}",true);
        exit();
    }



}